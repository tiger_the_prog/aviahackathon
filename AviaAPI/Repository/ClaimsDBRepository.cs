﻿using System;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using AviaAPI.Models;

namespace AviaAPI.Repository
{
    public class ClaimsDBRepository
    {
        private readonly ClaimsDBContext _context = null;

        public ClaimsDBRepository(IOptions<Settings> settings)
        {
            _context = new ClaimsDBContext(settings);
        }

        public async Task<IEnumerable<Claim>> GetAllClaims() 
        {
            return await _context.ClaimsCollection.Find(_ => true).ToListAsync();
        }

        public async Task<Claim> GetClaim(string claimId) 
        {
            var filter = Builders<Claim>.Filter.Eq("ClaimID", claimId);
            return await _context.ClaimsCollection.Find(filter).FirstOrDefaultAsync();
        }
        public async Task<bool>CheckClaimExists(string claimId)
        {
            var filter = Builders<Claim>.Filter.Eq("ClaimID", claimId);
            return (await _context.ClaimsCollection
                            .Find(filter)
                   .FirstOrDefaultAsync()) != null;
        }

        public async Task <bool> AddClaim(Claim claim) 
        {
            bool tmp = await CheckClaimExists(claim.ClaimID);
            if (!tmp)
            {
                await _context.ClaimsCollection.InsertOneAsync(claim);
            }
            return !tmp;
        }
        public async Task<bool>UpdateClaim(Claim claim)
        {
            var filter = Builders<Claim>.Filter.Eq("ClaimID", claim.ClaimID);
            return await _context.ClaimsCollection.ReplaceOneAsync(filter,claim) != null;        
        }
        public async Task<DeleteResult>RemoveUser(string claimId)
        {
            return await _context.ClaimsCollection.DeleteOneAsync(
                Builders<Claim>.Filter.Eq("ClaimID", claimId));
        }

    }
}
