﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Threading.Tasks;
using AviaAPI.Models;
namespace AviaAPI.Repository
{
    public class ClaimsDBContext
    {
        private readonly IMongoDatabase _database = null;

        public ClaimsDBContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<Claim> ClaimsCollection => _database.GetCollection<Claim>("ClaimsCollection");
    }
}
