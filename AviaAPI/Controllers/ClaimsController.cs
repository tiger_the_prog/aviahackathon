﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using AviaAPI.Models;
using AviaAPI.Repository;
using Newtonsoft.Json.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AviaAPI.Controllers
{
    [Route("api/[controller]")]
    public class ClaimsController : Controller
    {

        private readonly ClaimsDBRepository _rep;
        public ClaimsController(ClaimsDBRepository rep) 
        {
            _rep = rep;
        }
        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<Claim>> Get()
        {
            return await _rep.GetAllClaims();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<Claim> GetItem(string id)
        {
            return await _rep.GetClaim(id);
        }

        [HttpPost]
        public async Task<bool> Post([FromBody]string json)
        {
            if (json == null) 
            {
                return false;
            }
            Claim claim = JsonConvert.DeserializeObject<Claim>(json);
            return await _rep.AddClaim(claim);
        }
        [HttpPut]
        public async Task<bool> Put([FromBody] string json)
        {
            if (json == null)
            {
                return false;
            }
            Claim claim = JsonConvert.DeserializeObject<Claim>(json);
            return await _rep.UpdateClaim(claim);
        }
        

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
            
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
