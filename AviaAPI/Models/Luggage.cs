﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AviaAPI.Models
{
    public class Luggage
    {
        public string Tag { get; set; }
        public List<Obj> objs { get; set; }
    }
}
