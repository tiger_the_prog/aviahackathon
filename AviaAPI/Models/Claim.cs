﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace AviaAPI.Models
{
    public enum Reason {
        Delay = 1,
        LuggageLoss = 2,
        LuggageDamage = 3,
        DelayAndLuggageDamage = 4,
        DelayAndLuggageLoss = 5,
        DelayAndLuggageLossAndLuggageDamage = 6
    }

    public class Claim {
        public Claim()
        {
        }
        //public Claim()
        //[BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        [BsonIgnoreIfNull]
        [BsonId]
       
        public string ClaimID { get; set; }
        public Person Person { get; set; }
        public Flight Flight { get; set; }
        public Luggage Luggage { get; set; }
        public Reason Reason { get; set; }
    }
}
