﻿using System;
namespace AviaAPI.Models
{
    public class Test
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    }
}
