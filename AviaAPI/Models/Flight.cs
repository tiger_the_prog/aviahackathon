﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AviaAPI.Models
{
    public class Flight
    {
        public string FlightID { get; set; }
        public Tuple<DateTime, DateTime> FlightDates { get; set; } //item1 - start, item2 - end
        public Tuple<string, string> Route { get; set; } //item1 - from, item2 - to
        public string Price { get; set; } //"$3200.64"
    }
}
