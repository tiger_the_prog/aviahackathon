﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AviaAPI.Models
{
    public class Obj
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public string Price { get; set; }
    }
}
