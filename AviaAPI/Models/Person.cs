﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AviaAPI.Models
{
    public enum Sex
    {
        Male = 1,
        Female = 2
    }
    public class Person
    {
        public Person() {
            
        }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public Sex Sex { get; set; }
        public string Citizenship { get; set; }
        public string PhotoSummary { get; set; }
        public string MessengerLogin { get; set; } // Local channel login
        public string MessengerName { get; set; } // username in channel with @
    }
}
